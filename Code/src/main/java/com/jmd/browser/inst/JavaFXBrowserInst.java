package com.jmd.browser.inst;

import com.jmd.browser.inst.base.AbstractBrowser;
import com.jmd.callback.CommonAsyncCallback;
import com.jmd.callback.JavaScriptExecutionCallback;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;

public class JavaFXBrowserInst extends AbstractBrowser {

	static {
		Platform.setImplicitExit(false);
	}

	private static volatile JavaFXBrowserInst instance;

	private static WebView webView = null;
	private JFXPanel container = null;

	@Override
	public void create(String url, CommonAsyncCallback callback) {
		if (webView == null) {
			Platform.startup(() -> {
				_create(url, callback);
			});
		} else {
			Platform.runLater(() -> {
				_create(url, callback);
			});
		}
	}

	private void _create(String url, CommonAsyncCallback callback) {
		webView = new WebView();
		webView.setMouseTransparent(false);
		webView.setContextMenuEnabled(false);
		webView.getEngine().load(url);
		container = new JFXPanel();
		container.setScene(new Scene(webView));
		callback.execute();
	}

	@Override
	public WebView getBrowser() {
		return webView;
	}

	@Override
	public JFXPanel getContainer() {
		return this.container;
	}

	@Override
	public String getVersion() {
		return "JavaFX 16 WebView WebEngine, WebKit, " + webView.getEngine().getUserAgent();
	}

	@Override
	public void loadURL(String url) {
		Platform.runLater(() -> {
			webView.getEngine().load(url);
		});
	}

	@Override
	public void reload() {
		Platform.runLater(() -> {
			webView.getEngine().reload();
		});
	}

	@Override
	public void dispose(int a) {
		Platform.runLater(() -> {
			webView.getEngine().load(null);
			container.removeAll();
			container = null;
			System.gc();
		});
	}

	@Override
	public void clearLocalStorage() {
		Platform.runLater(() -> {
			this.execJS("localStorage.removeItem(\"jmd-config\")");
		});
	}

	@Override
	public void sendShared(String topic, String message) {
		Platform.runLater(() -> {
			this.execJS("sharedService.pub(\"" + topic + "\", \"" + (null == message ? "" : message) + "\")");
		});
	}

	@Override
	public void execJS(String javaScript) {
		Platform.runLater(() -> {
			webView.getEngine().executeScript(javaScript);
		});
	}

	@Override
	public void execJSWithStringBack(String javaScript, JavaScriptExecutionCallback callback) {
		Platform.runLater(() -> {
			Object res = webView.getEngine().executeScript(javaScript);
			callback.execute(res.toString());
		});
	}

	public static JavaFXBrowserInst getIstance() {
		if (instance == null) {
			synchronized (JavaFXBrowserInst.class) {
				if (instance == null) {
					instance = new JavaFXBrowserInst();
				}
			}
		}
		return instance;
	}

}
