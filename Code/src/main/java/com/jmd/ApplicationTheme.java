package com.jmd;

import javax.annotation.PostConstruct;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.jmd.callback.CommonAsyncCallback;
import com.jmd.common.Setting;
import com.jmd.ui.AboutFrame;
import com.jmd.ui.DonateFrame;
import com.jmd.ui.LicenseFrame;
import com.jmd.ui.MainFrame;
import com.jmd.ui.MainMenuBar;
import com.jmd.ui.ProxySettingFrame;
import com.jmd.ui.tab.a_mapview.DownloadConfigFrame;
import com.jmd.ui.tab.a_mapview.DownloadPreviewFrame;
import com.jmd.util.CommonUtils;

@Lazy
@Component
public class ApplicationTheme {

	private static final boolean isWindows10 = CommonUtils.isWindows10();

	@Autowired
	private MainMenuBar mainMenuBar;
	@Autowired
	private MainFrame mainFrame;
	@Autowired
	private AboutFrame aboutFrame;
	@Autowired
	private LicenseFrame licenseFrame;
	@Autowired
	private DonateFrame donateFrame;
	@Autowired
	private ProxySettingFrame proxySettingFrame;
	@Autowired
	private DownloadConfigFrame downloadConfigFrame;
	@Autowired
	private DownloadPreviewFrame downloadPreviewFrame;

	public static void init() {
		try {
			if (isWindows10) {
				// Win10直接支持窗口装饰
				UIManager.setLookAndFeel(ApplicationSetting.getSetting().getThemeClazz());
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);
			} else {
				// 其他系统需要先通过jtattoo触发
				UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@PostConstruct
	private void postInit() {
		Setting setting = ApplicationSetting.getSetting();
		mainMenuBar.getThemeNameLabel().setText("Theme: " + setting.getThemeName());
	}

	public void change(String name, String clazz, CommonAsyncCallback callback) {
		SwingUtilities.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(clazz);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			mainMenuBar.getThemeNameLabel().setText("Theme: " + name);
			updateUI(callback);
			Setting setting = ApplicationSetting.getSetting();
			setting.setThemeName(name);
			setting.setThemeClazz(clazz);
			ApplicationSetting.save(setting);
		});
	}

	public void updateUI(CommonAsyncCallback callback) {
		SwingUtilities.invokeLater(() -> {
			SwingUtilities.updateComponentTreeUI(mainFrame);
			SwingUtilities.updateComponentTreeUI(aboutFrame);
			SwingUtilities.updateComponentTreeUI(licenseFrame);
			SwingUtilities.updateComponentTreeUI(donateFrame);
			SwingUtilities.updateComponentTreeUI(proxySettingFrame);
			SwingUtilities.updateComponentTreeUI(downloadConfigFrame);
			SwingUtilities.updateComponentTreeUI(downloadPreviewFrame);
			if (callback != null) {
				callback.execute();
			}
		});
	}

}
