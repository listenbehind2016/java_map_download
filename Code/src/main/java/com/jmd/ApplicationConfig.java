package com.jmd;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;

import com.jmd.util.ConnectorUtils;

@Configuration
public class ApplicationConfig implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

	public static int startPort = 0;

	@Override
	public void customize(ConfigurableServletWebServerFactory factory) {
		((TomcatServletWebServerFactory) factory).addConnectorCustomizers(new TomcatConnectorCustomizer() {
			@Override
			public void customize(Connector connector) {
				// 获取可用端口，指定端口范围，如果返回-1则范围内没有可用的，此时会使用80端口
				int port = ConnectorUtils.findAvailablePort(26737, 26787);
				try {
					if (port < 0) {
						throw new Exception("no available port !");
					} else {
						startPort = port;
						connector.setPort(port);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
